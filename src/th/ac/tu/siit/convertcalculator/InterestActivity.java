package th.ac.tu.siit.convertcalculator;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class InterestActivity extends Activity implements OnClickListener {

	float interestRate;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_interest);
		
		Button b = (Button)findViewById(R.id.calculate);
		b.setOnClickListener(this);
		
		Button b1 = (Button)findViewById(R.id.setting);
		b1.setOnClickListener(this);
		
		TextView interestRateTV = (TextView)findViewById(R.id.interestRate);
		interestRate = Float.parseFloat(interestRateTV.getText().toString());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.interest, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		int id = v.getId();
		
		if(id == R.id.calculate)
		{
			EditText pET = (EditText)findViewById(R.id.p);
			EditText tET = (EditText)findViewById(R.id.t);
			TextView reTV = (TextView)findViewById(R.id.result);
			
			String result = "";
			
			try {
				float p = Float.parseFloat(pET.getText().toString());
				float t = Integer.parseInt(tET.getText().toString());
				float tmp_result = p*(float) Math.pow((1+(interestRate/100)), t);
				result = String.format(Locale.getDefault(), "%.3f", tmp_result);
			} catch(NumberFormatException e1) {
				result = "Invalid input";
			} catch(NullPointerException e2) {
				result = "Invalid input";
			}
			
			reTV.setText(result);
		}
		else if(id == R.id.setting)
		{
			Intent i = new Intent(this, SettingActivity.class);
			i.putExtra("interestRate", interestRate);
			startActivityForResult(i, 8888);
		}
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 8888 && resultCode == RESULT_OK) {
			interestRate = data.getFloatExtra("interestRate", 32.0f);
			TextView intRate = (TextView)findViewById(R.id.interestRate);
			intRate.setText(String.format(Locale.getDefault(), "%.2f", interestRate));
		}
	}

}
